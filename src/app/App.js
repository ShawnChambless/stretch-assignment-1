import React from 'react';
import DataNode from './DataNode';
import _ from 'lodash';
import '../styles/app.css';

const App = () => {
	const dataNodes = _.map(_.range(10000), (val, ind) => <DataNode key={ ind } index={ ind }/>);
	return (
			<div className='data-nodes'>
				{ dataNodes }
			</div>
	);
};

export default App;