import React, { Component } from 'react';

class DataNode extends Component {
	
	constructor({ index }) {
		super();
		this.state = {
			index:            index
			, clicked:        false
			, numberOfClicks: 0
			, red:            255
			, green:          255
			, blue:           255
			, color:          '#333'
		};
	}
	
	handleClick() {
		const incClicks            = clicks => clicks + 1;
		const recalculateColor     = () => {
			const getNumber = () => Math.round(Math.random() * 255);
			return {
				red:     getNumber()
				, green: getNumber()
				, blue:  getNumber()
			};
		};
		const { red, green, blue } = recalculateColor();
		const color                = Math.round(((red * 299) + (green * 587) + (blue * 114)) / 1000) > 125 ? '#333' : '#fff';
		
		this.setState(prevState => ({
			clicked:          true
			, numberOfClicks: incClicks(prevState.numberOfClicks)
			, red: red
			, green
			, blue
			, color
		}));
	}
	
	componentWillUpdate() {
		console.log(`Updating node at index ${this.state.index}.`);
	}
	
	render() {
		const styles = {
			dataNode:       {
				background: `rgb(${this.state.red}, ${this.state.green}, ${this.state.blue})`
			}
			, dataNodeData: {
				color: this.state.color
			}
		};
		
		return (
				<div className='data-node' style={ styles.dataNode }
						 onClick={ this.handleClick.bind(this) }>
					<div className='data-node-data' style={ styles.dataNodeData }>{ this.state.numberOfClicks }</div>
				</div>
		);
	}
}

export default DataNode;