FROM node:8.2.1

WORKDIR /usr/src/app

COPY . /usr/src/app/

RUN yarn --prod

RUN yarn build

RUN yarn global add serve

CMD [ "serve", "-s", "build"]
