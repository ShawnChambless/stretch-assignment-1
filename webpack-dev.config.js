const path              = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
	template: './public/index.html',
	filename: 'index.html',
	inject:   'body'
});

module.exports = {
	entry:   './src/index.js',
	output:  {
		path:     path.resolve('./build'),
		filename: 'bundle.js'
	},
	module:  {
		rules: [
			{
				test:      /\.js$/
				, use:     'babel-loader'
				, exclude: /node_modules/
			},
			{
				test: /\.css$/,
				use:  [ 'style-loader', 'css-loader', 'postcss-loader' ]
			}
		]
	},
	plugins: [ HtmlWebpackPluginConfig ]
	
};
